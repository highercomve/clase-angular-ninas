var angular = require('angular')

// set
angular.module('clase3', [])

// get
var miModulo = angular.module('clase3')

miModulo.run(function() {
  console.log("Angular esta listo papa")
})

miModulo.controller('NombreControlador', function($scope) {
  this.time = 10
  this.interval = null

  this.start = () => {
    if(this.interval !== null) {
      this.interval = setInterval(this.count.bind(this), 1000 )
    }
  }

  this.count = () => {
    if(this.time > 0) {
      this.time -= 1
      $scope.$apply()
      console.log(this.time)
    } else {
      clearInterval(this.interval)
    }
  }
})

// miModulo.directive('nombreDirectiva', function() {})
// miModulo.factory('NombreFactory', function() {})
// miModulo.service('NombreService', function() {})

angular.module('App', ['clase3'])
