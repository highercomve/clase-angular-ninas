import $ from 'jquery'

class Timer {
  constructor(element) {
    this.started = false
    this.element = $(element)
    this.interval = false
    this.count = 0
  }

  down() {
    this.count += -1
    console.info('Time is', this.count)

    if(this.count <= 0) {
      this.stop()
    } else {
      this.element.val(this.count)
    }
  }

  stop() {
    console.info('Stop the Counter')
    clearInterval(this.interval)
    this.interval = false
    this.element.val(this.count)
  }

  continue() {
    console.log(this.initial)
    if(this.initial !== 0 && !Number.isNaN(this.initial)) {
      console.info('Continue the timer')
      this.interval = setInterval(this.down.bind(this), 1000)
    } else {
      alert('Debe iniciar el contador con un valor')
    }
  }

  toggle() {
    if(this.interval) {
      this.stop()
    } else {
      console.log(this.started)
      if(this.started === false) {
        this.start()
      }
      this.continue()
    }
  }

  start() {
    console.log('restart')
    this.started = true
    this.initial = parseInt(this.element.val())
    this.count = this.initial
  }

  restart() {
    this.count = this.initial
    this.element.val(this.count)
  }
}



export default Timer
