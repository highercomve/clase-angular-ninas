import $ from 'jquery'
import Timer from './timer'

$(document).ready(function() {
  var initial = $('time')
  var timer = new Timer('.time')
  console.log(timer)
  $("button[data-type='start']").click(function() {
    timer.toggle()
    if(timer.interval) {
      $(this).html('Pause')
    } else {
      $(this).html('Start')
    }
  })

  $("button[data-type='reset']").click(function() {
    timer.restart()
  })

})
