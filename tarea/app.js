var $ = require('jquery')

$(document).ready(() => {
  var allContent = $('li div')
  var allTitles = $('li h3')

  allContent.hide()

  allTitles.click(function() {
    allContent.hide()
    $(this).next().toggle()
  })
})
